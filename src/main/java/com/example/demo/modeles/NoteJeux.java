package com.example.demo.modeles;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@NoArgsConstructor
public class NoteJeux {

	@NonNull
	private String pseudo_user;

	@NonNull
	private int id_jeux;

	@NonNull
	private double note;

	@NonNull
	private String commentaire;

	@NonNull
	private String date;
	
	private double moyenne;

	public NoteJeux(int id_jeux, double moyenne) {
		super();
		this.id_jeux = id_jeux;
		this.moyenne = moyenne;
	}
	
	
	
}
