package com.example.demo.modeles;

public class Mail {


	 public final static String MAIL_DE_BIENVENUE ="<div style='background-color: #418EFF;'><img src='http://image.noelshack.com/fichiers/2019/02/3/1547057851-logo.png' alt='logo' width='25%'/></div>"
	 		                               + "<h1>Bienvenue sur Apprendre en s'amusant</h1>"
	 									   + "<p style='margin-top:10px;'>Nous sommes heureux de vous compter parmis nos abonnés,<br>"
	 									   + "vous pouvez à présent acceder à tout les jeux, spécialements conçus pour permettre aux enfants d'apprendre"
	 									   + "tout en s'amusant.</p>"
	 									   + "<p>Coridlament</p>"
	 									   + "<p>L'équipe d'apprendre en s'amusant</p>";
	 
	 public final static String MAIL_PASSWORD_DEBUT = "<div style='background-color: #418EFF;'><img src='http://image.noelshack.com/fichiers/2019/02/3/1547057851-logo.png' alt='logo' width='25%'/></div>"  
			                                         + "<h1>Bonjour</h1>"
	 												 + "<p style='margin-top: 10px;'>Vous venez de faire une demande de réinitialisation de mot de passe, voici votre code :</p>"
	 												 + "<p style='margin-top:10px; margin-left:100px; font-weight: bold; text-decoration: underline; font-size:24px;'>";
	 
	 public final static String MAIL_PASSWORD_FIN =  "</p>"
	 											   + "<p style='margin-top:10px;'>Si vous n'avez pas fait cette demande, ne tenez pas compte de ce mail"
	 											   + "<p>Coridlament</p>"
	 											   + "<p>L'équipe d'apprendre en s'amusant</p>";
	 
	 public final static String MAIL_ACHAT_DEBUT = "<head><meta charset='UTF-8'></head>"
	 											+ "<div style='background-color: #418EFF;'><img src='http://image.noelshack.com/fichiers/2019/02/3/1547057851-logo.png' alt='logo' width='25%'/></div>"  
             									+ "<h1>Bonjour</h1>"
 												+ "<p style='margin-top: 10px;'>Vous venez d'effectuer l'achat suivant:</p>"
 												+ "<table style='border: 1px solid black'>"
 												+ "<thead>"
 												+ "<tr>"
 												+ "<th style='border-right: 1px solid black; border-bottom: 1px solid black; text-align:center; padding: 10px 20px'>Description</th>"
 												+ "<th style='text-align:center; padding: 10px 20px; border-bottom: 1px solid black;'>Prix</th>"
 												+ "</tr>"
 												+ "</thead>"
 												+ "<tbody>"
 												+ "<tr>"
 												+ "<td style='border-right: 1px solid black; text-align:center; padding: 10px 20px'>";
 												 
	 public final static String MAIL_ACHAT_MILIEU = "</td>"
 												  + "<td style='text-align:center; padding: 10px 20px'>";
 												  
	 public final static String MAIL_ACHAT_FIN = ",00</td>"
	 											+ "</tr>"
 												+ "</tbody>"
 												+ "</table>"
 												+"<p style='margin-top:10px;'>Nous vous remercions pour votre confiance et vous souhaitons un bon amusement.</p>"
			   									+ "<p>Coridlament</p>"
		   										+ "<p>L'équipe d'apprendre en s'amusant</p>";
}
