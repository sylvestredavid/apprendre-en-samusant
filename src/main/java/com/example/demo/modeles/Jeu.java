package com.example.demo.modeles;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Jeu{

	private int id;
	
	private String nom;
	
	private String photo;
	
	private String lien;
	
	private int prix;
	
	private String description;
	
	private Categorie categorie;
	
	private double moyenne;

	
	
	
	
}
