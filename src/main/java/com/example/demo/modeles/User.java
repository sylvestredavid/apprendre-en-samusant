package com.example.demo.modeles;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@NoArgsConstructor
public class User {


	@NonNull
	private int id;
	
	@NotEmpty
	@NonNull
	private String pseudo;
	
	@NotEmpty
	@NonNull
	private String password;
	
	@NotEmpty
	@NonNull
	private String email;
	
	private String password2;
	
	private int code;
	
	private int codeAVerifier;


	
	
}
