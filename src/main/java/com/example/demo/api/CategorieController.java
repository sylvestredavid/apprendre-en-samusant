package com.example.demo.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.modeles.Categorie;
import com.example.demo.services.ICategorieService;

@Controller
public class CategorieController {

	@Autowired
	ICategorieService categorieService;
	
	@GetMapping("menu")
	public String menu(Model model) {
		List<Categorie> categories = categorieService.findAllCategorie();
		model.addAttribute("categories", categories);
		return "menu";
	}

	@PostMapping("menu")
	public String menuPost(Model model) {
		List<Categorie> categories = categorieService.findAllCategorie();
		model.addAttribute("categories", categories);
		return "menu";
	}
}
