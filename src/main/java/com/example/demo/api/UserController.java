package com.example.demo.api;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.servlet.http.Cookie;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import com.example.demo.modeles.User;
import com.example.demo.services.IEmailService;
import com.example.demo.services.IUserService;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;

@Controller
public class UserController {

	@Autowired
	IUserService userService;
	
	@Autowired
	IEmailService sendMail;
	

	@GetMapping("/about")
	public String about() {
		return "about";
	}

	@GetMapping("/logon")
	public String logonForm(Model model) {
		model.addAttribute("user", new User());
		return "logon";
	}

	@PostMapping("/logon")
	public String UserSubmit(@ModelAttribute("user") @Valid User user, BindingResult result, WebRequest request, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("erreur", "Tout les champs doivent etres remplis");
			return "logon";
		}
		else if(!userService.findOneUserByPseudo(user.getPseudo()).isEmpty()) {
			model.addAttribute("erreur", "ce pseudo est deja pris");
			return "logon";
		}
		else if(!userService.findOneUserByMail(user.getEmail()).isEmpty()) {
			model.addAttribute("erreur", "un compte existe deja avec cet email");
			return "logon";
		}
		else if(!userService.verifyPassword(user.getPassword())) {
			model.addAttribute("erreur", "le mot de passe doit contenir 8 characteres dont au moins 1 majuscule et 1 chiffre");
			return "logon";
		}
		else if(!user.getPassword().equals(user.getPassword2())) {
			model.addAttribute("erreur", "les mots de passes ne sont pas identiques");
			return "logon";
		}
		userService.saveUser(user);
		request.setAttribute("userSession", userService.findOneUserByPseudo(user.getPseudo()).get(0), WebRequest.SCOPE_SESSION);
		sendMail.WelcomeMail(user.getEmail());
		return "index";
	}

	@GetMapping("/login")
	public String loginForm(Model model) {
		model.addAttribute("user", new User());
		return "login";
	}

	@PostMapping("/login")
	public String loginSubmit(@ModelAttribute("user") @Valid User user, BindingResult result, WebRequest request, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("erreur", "Tout les champs doivent etres remplis");
			return "login";
		}
		else if(!userService.verifyUser(user)) {
			model.addAttribute("erreur", "les identifiants ne sonts pas valides");
			return "login";
		}
		User userSession = userService.findOneUserByPseudo(user.getPseudo()).get(0);
		request.setAttribute("userSession", userSession, WebRequest.SCOPE_SESSION);
		
		return "index";
	}

	@GetMapping("/logout")
	public String disconnect(WebRequest request) {
		request.removeAttribute("userSession", WebRequest.SCOPE_SESSION);
		return "index";
	}
	
	@GetMapping("/forget")
	public String forgetPassword(Model model) {
		model.addAttribute("user", new User());
		return "forget";
	}
	
	@GetMapping("/forgot")
	public String forgotPassword(Model model, @RequestParam(value="email") String email) {
		Random rand = new Random();
		int code = rand.nextInt(999999 - 100000+1)+100000;
		sendMail.forgetMail(email, code);
		int codeAAficher = code*13546565+476445465; 
		model.addAttribute("code", codeAAficher);
		model.addAttribute("email", email);
		model.addAttribute("user", new User());
		return "forgot";
	} 
	
	@PostMapping("/newPassword")
	public String forgotPasswordPost(Model model, @ModelAttribute("user")  User user) {
		if((user.getCode()*13546565+476445465) == user.getCodeAVerifier()) {
			model.addAttribute("email", user.getEmail());
			return "newPassword";
		}
		return "newPassword";
	}  
	
	@PostMapping("/passwordChanged")
	public String changePassword(@ModelAttribute("user")  User user) {
		userService.changePassword(user);
		return "passwordChanged";
	}  
	
	@GetMapping("/contact")
	public String contact() {
		return "contact";
	}
	
	@PostMapping("/contact")
	public String contactPost(WebRequest request, Model model) {
		String mailFrom = request.getParameter("mailFrom");
		String subject = request.getParameter("subject");
		String contenu = request.getParameter("contenu");
		boolean mailEnvoye = sendMail.contactMail(mailFrom, subject, contenu);
		if(mailEnvoye) {
			model.addAttribute("resultat", "Votre demande a bien été envoyée, nous vous repondrons dès que possible.");
		}
		else {
			model.addAttribute("resultat", "il y a eu une erreur dans l'envoi du mail, merci de réessayer.");
		}
		return "contact";
	}
	
	@PostMapping("/googleSignIn")
	public String googleSignIn(WebRequest request) throws GeneralSecurityException, IOException {

		GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance())
		    // Specify the CLIENT_ID of the app that accesses the backend:
		    .setAudience(Collections.singletonList("657601800153-qmbojlb2etaqggv09tiim4ofv911d22o.apps.googleusercontent.com"))
		    // Or, if multiple clients access the backend:
		    //.setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
		    .build();

		// (Receive idTokenString by HTTPS POST)

		GoogleIdToken idToken = verifier.verify(request.getParameter("idtoken"));
		if (idToken != null) {
		  Payload payload = idToken.getPayload();

		  // Print user identifier
		  String userId = payload.getSubject();
		  // Get profile information from payload
		  String email = payload.getEmail();
		  User user = new User();
		  user.setPseudo(email.split("@")[0]);
		  user.setEmail(email);
		  user.setPassword(userId);
		  List<User> users = userService.findOneUserByMail(user.getEmail());
		  if(users.isEmpty()) {
			  userService.saveUser(user);
			  sendMail.WelcomeMail(user.getEmail());
		  }
		  else {
			  user = users.get(0);
		  }
		  request.setAttribute("userSession", user, WebRequest.SCOPE_SESSION);

		} else {
		  System.out.println("Invalid ID token.");
		}
		return "apprendre en s'amusant";
	}
	@PostMapping("/facebookSignIn")
	public String facebookSignIn(WebRequest request) throws GeneralSecurityException, IOException {
		FacebookClient fb = new DefaultFacebookClient(request.getParameter("idtoken"), Version.VERSION_2_10);
		
		com.restfb.types.User fbUser = fb.fetchObject("me", com.restfb.types.User.class, Parameter.with("fields", "email"));
		User user = new User();
		user.setPseudo(fbUser.getEmail().split("@")[0]);
		user.setPassword(fbUser.getId());
		user.setEmail(fbUser.getEmail());
		  List<User> users = userService.findOneUserByMail(user.getEmail());
		  if(users.isEmpty()) {
			  userService.saveUser(user);
			  sendMail.WelcomeMail(user.getEmail());
		  }
		  else {
			  user = users.get(0);
		  }
		  request.setAttribute("userSession", user, WebRequest.SCOPE_SESSION);
		return "index";
	}
}
