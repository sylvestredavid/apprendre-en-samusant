package com.example.demo.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import com.example.demo.modeles.AchatJeu;
import com.example.demo.modeles.Jeu;
import com.example.demo.modeles.NoteJeux;
import com.example.demo.modeles.User;
import com.example.demo.services.IJeuxService;
import com.example.demo.services.impl.EmailService;
import com.mysql.jdbc.StringUtils;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;

@Controller
public class JeuxController {

	@Autowired
	IJeuxService jeuService;

	@GetMapping("/")
	public String welcome(WebRequest request) {
		request.setAttribute("jeuALaUne", jeuService.topJeux().get(0), WebRequest.SCOPE_SESSION);
		List<Jeu> topJeux = jeuService.topJeux();
		List<Jeu> topJeuxAAfficher = new ArrayList<>();
		for (int i = 1; i < topJeux.size(); i++) {
			topJeuxAAfficher.add(topJeux.get(i));
		}
		request.setAttribute("topJeux", topJeuxAAfficher, WebRequest.SCOPE_SESSION);
		return "index";
	}

	@GetMapping("/index")
	public String index(WebRequest request) {
		request.setAttribute("jeuALaUne", jeuService.topJeux().get(0), WebRequest.SCOPE_SESSION);
		List<Jeu> topJeux = jeuService.topJeux();
		List<Jeu> topJeuxAAfficher = new ArrayList<>();
		for (int i = 1; i < topJeux.size(); i++) {
			topJeuxAAfficher.add(topJeux.get(i));
		}
		request.setAttribute("topJeux", topJeuxAAfficher, WebRequest.SCOPE_SESSION);
		return "index";
	}

	@GetMapping("/jeux")
	public String jeux(Model model, @RequestParam(value = "cat", required = false) String cat) {
		if (StringUtils.isNullOrEmpty(cat)) {
			model.addAttribute("jeux", jeuService.findAllJeux());
		} else {
			List<Jeu> jeux = jeuService.findJeuByCat(Integer.parseInt(cat));
			model.addAttribute("categorie", jeux.get(0).getCategorie().getNom());
			model.addAttribute("jeux", jeux);
		}
		return "jeux";
	}

	@PostMapping("/jeu")
	public void likeSubmit(WebRequest request) throws InterruptedException {
		SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yy");
		NoteJeux noteJeux = new NoteJeux();
		noteJeux.setPseudo_user(request.getParameter("pseudo_user"));
		noteJeux.setId_jeux(Integer.parseInt(request.getParameter("id_jeux")));
		noteJeux.setNote(Double.parseDouble(request.getParameter("note")));
		noteJeux.setCommentaire(request.getParameter("commentaire"));
		noteJeux.setDate(formater.format(new Date()));
		jeuService.noteJeu(noteJeux);
	}

	@GetMapping("/jeu")
	public String jeuxForm(Model model, @RequestParam(value = "nom") String nom, WebRequest request) {
		User user = (User) request.getAttribute("userSession", WebRequest.SCOPE_SESSION);
		Jeu jeu = jeuService.findJeuByLien(nom);
		if(user != null) {
			List<NoteJeux> notes = jeuService.dejaNote(user.getPseudo(), jeu.getId());
			if(!notes.isEmpty()) {
				model.addAttribute("note", notes.get(0));
			}
		}
		List<NoteJeux> commentaires = jeuService.getNotesByJeux(jeu.getId());
		List<Jeu> autres = jeuService.findJeuByCat(jeu.getCategorie().getId());
		List<Jeu> autresJeux = new ArrayList<>();
		for (Jeu jeu2 : autres) {
			if (jeu2.getId() != jeu.getId()) {
				autresJeux.add(jeu2);
			}
		}
		if (jeu.getPrix() > 0) {
			if (jeuService.IsjeuAchete(user, jeu)) {
				if (user != null) {
					model.addAttribute("isJeuPref", true);
				}
				model.addAttribute("jeu", jeu);
				model.addAttribute("jeuxPref", new NoteJeux());
				model.addAttribute("autresJeux", autresJeux);
				model.addAttribute("commentaires", commentaires);
				return "jeu";
			} else {
				model.addAttribute("jeu", jeu);
				model.addAttribute("autresJeux", autresJeux);
				model.addAttribute("commentaires", commentaires);
				return "paiement";
			}
		} else {
			if (user != null) {
				model.addAttribute("isJeuPref", true);
			}
			model.addAttribute("jeu", jeu);
			model.addAttribute("jeuxPref", new NoteJeux());
			model.addAttribute("autresJeux", autresJeux);
			model.addAttribute("commentaires", commentaires);
			return "jeu";
		}
	}

	@GetMapping("/mesJeux")
	public String jeuxPref(Model model, WebRequest request) {
		User user = (User) request.getAttribute("userSession", WebRequest.SCOPE_SESSION);
		List<Jeu> mesJeux = jeuService.findJeuxByUser(user);
		model.addAttribute("mesJeux", mesJeux);
		return "mesJeux";
	}
	
	@PostMapping("/paiement")
	public String paiementFait(WebRequest request, Model model){
		Stripe.apiKey = "sk_test_7rkSP1nq1oILBmkYxBnRIDeO";

		// Token is created using Checkout or Elements!
		// Get the payment token ID submitted by the form:
		String token = request.getParameter("stripeToken");

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("amount", 100);
		params.put("currency", "eur");
		params.put("description", request.getParameter("description"));
		params.put("source", token);
		try {
			Charge charge = Charge.create(params);
			AchatJeu achatJeu = new AchatJeu(Integer.parseInt(request.getParameter("id_user")), Integer.parseInt(request.getParameter("id_jeux")));
			jeuService.achat(achatJeu);
			Jeu jeu = jeuService.findOneJeu( Integer.parseInt(request.getParameter("id_jeux")));
			model.addAttribute("jeu", jeu);
			EmailService email = new EmailService();
			email.achatMail(jeu.getNom(), jeu.getPrix()/100, request.getParameter("stripeEmail"));
			return "/accepted";
		} catch (StripeException e) {
			e.printStackTrace();
			return "/declined";
		}
		
	}

}
