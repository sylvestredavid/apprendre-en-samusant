package com.example.demo.services;

import java.util.List;

import com.example.demo.modeles.AchatJeu;
import com.example.demo.modeles.Jeu;
import com.example.demo.modeles.NoteJeux;
import com.example.demo.modeles.User;

public interface IJeuxService {

	public List<Jeu> findAllJeux();
	
	public Jeu findOneJeu(int id);
	
	public Jeu findJeuByLien(String lien);
	
	public boolean IsjeuAchete(User user, Jeu jeu);
	
	public List<Jeu> topJeux();

	List<Jeu> findJeuByCat(int cat);

	void achat(AchatJeu achatJeu);

	List<NoteJeux> getNotesByJeux(int id_jeu);

	List<Jeu> findJeuxByUser(User user);

	void noteJeu(NoteJeux noteJeux);

	List<NoteJeux> dejaNote(String pseudo_user, int id_jeux);
}
