package com.example.demo.services;

import java.util.List;

import com.example.demo.modeles.User;

public interface IUserService {

	public List<User> findAll();
	
	public List<User> findOneUserById(int id);
	
	public List<User> findOneUserByPseudo(String pseudo);
	
	public List<User> findOneUserByMail(String mail);
	
	public User saveUser(User user);
	
	public void changePassword(User user);
	
	public boolean verifyUser(User user);
	
	public boolean verifyPassword(String password);
}
