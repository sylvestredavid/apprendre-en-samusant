package com.example.demo.services;

import java.util.List;

import com.example.demo.modeles.Categorie;

public interface ICategorieService {

	List<Categorie> findAllCategorie();
	
	Categorie findCategorieByName(String name);
	
	List<Categorie> findCategorieById(int id);
}
