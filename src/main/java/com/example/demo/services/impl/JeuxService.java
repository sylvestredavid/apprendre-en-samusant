package com.example.demo.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.example.demo.mapping.AchatJeuRowMapper;
import com.example.demo.mapping.JeuRowMapper;
import com.example.demo.mapping.MoyenneRowMapper;
import com.example.demo.mapping.NoteJeuxRowMapper;
import com.example.demo.modeles.AchatJeu;
import com.example.demo.modeles.Jeu;
import com.example.demo.modeles.NoteJeux;
import com.example.demo.modeles.User;
import com.example.demo.services.IJeuxService;

@Service
public class JeuxService implements IJeuxService {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public List<Jeu> findAllJeux() {
		return jdbcTemplate.query(
				"SELECT jeux.id, jeux.nom, photo, lien, prix, description, moyenne, categories.id, categories.nom FROM jeux INNER JOIN categories ON jeux.categorie = categories.id order by moyenne desc",
				new JeuRowMapper());
	}

	@Override
	public Jeu findOneJeu(int id) {
		return jdbcTemplate.query(
				"SELECT jeux.id, jeux.nom, photo, lien, prix, description, moyenne, categories.id, categories.nom FROM jeux INNER JOIN categories ON jeux.categorie = categories.id WHERE jeux.id = ?",
				new Object[] { id }, new JeuRowMapper()).get(0);
	}

	@Override
	public Jeu findJeuByLien(String lien) {
		return jdbcTemplate.query(
				"SELECT jeux.id, jeux.nom, photo, lien, prix, description, moyenne, categories.id, categories.nom FROM jeux INNER JOIN categories ON jeux.categorie = categories.id WHERE lien = ?",
				new Object[] { lien }, new JeuRowMapper()).get(0);
	}

	@Override
	public List<Jeu> findJeuByCat(int cat) {
		return jdbcTemplate.query(
				"SELECT jeux.id, jeux.nom, photo, lien, prix, description, moyenne, categories.id, categories.nom FROM jeux INNER JOIN categories ON jeux.categorie = categories.id WHERE categorie = ? order by moyenne desc",
				new Object[] { cat }, new JeuRowMapper());
	}

	@Override
	public List<Jeu> topJeux() {
		List<NoteJeux> notesJeux =  jdbcTemplate.query("SELECT id_jeux, round((round(avg(note)*2)/2),1) as moyenne FROM notes_jeux group by id_jeux order by moyenne desc", new MoyenneRowMapper());
		List<Jeu> topJeux = new ArrayList<>();
		for (NoteJeux note : notesJeux) {
			topJeux.add(jdbcTemplate.query("SELECT jeux.id, jeux.nom, photo, lien, prix, description, moyenne, categories.id, categories.nom FROM jeux INNER JOIN categories ON jeux.categorie = categories.id WHERE jeux.id = ?", new Object[] { note.getId_jeux() }, new JeuRowMapper()).get(0));
			jdbcTemplate.update("UPDATE jeux SET moyenne=? WHERE id = ?",note.getMoyenne(), note.getId_jeux());
		}
		return topJeux;
	}

	@Override
	public boolean IsjeuAchete(User user, Jeu jeu) {
		if (user != null) {
			List<AchatJeu> achatJeu = jdbcTemplate.query(
					"SELECT id_user, id_jeux FROM achat_jeux WHERE id_user = ? AND id_jeux = ?",
					new Object[] { user.getId(), jeu.getId() }, new AchatJeuRowMapper());

			if (achatJeu.isEmpty()) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	@Override
	public void achat(AchatJeu achatJeu) {
		jdbcTemplate.update("insert into achat_jeux(id_user, id_jeux) VALUES(?,?)", achatJeu.getId_user(),
				achatJeu.getId_jeux());
	}
	
	@Override
	public List<Jeu> findJeuxByUser(User user){
		return jdbcTemplate.query("SELECT jeux.id, jeux.nom, photo, lien, prix, description, moyenne, categories.id, categories.nom FROM jeux INNER JOIN categories ON jeux.categorie = categories.id inner join achat_jeux on jeux.id= achat_jeux.id_jeux where achat_jeux.id_user=?", new Object[]{ user.getId() }, new JeuRowMapper() );
	}
	
	@Override
	public List<NoteJeux> getNotesByJeux(int id_jeu) {
		return jdbcTemplate.query("SELECT * FROM notes_jeux where id_jeux = ? order by id desc",new Object[] { id_jeu}, new NoteJeuxRowMapper());
	}
	
	@Override
	public void noteJeu(NoteJeux noteJeux) {
		jdbcTemplate.update("insert into notes_jeux(pseudo_user, id_jeux, note, commentaire, datecom) values(?,?,?,?,?);",noteJeux.getPseudo_user(), noteJeux.getId_jeux(), noteJeux.getNote(), noteJeux.getCommentaire(), noteJeux.getDate());
		topJeux();
	}
	
	@Override
	public List<NoteJeux> dejaNote(String pseudo_user, int id_jeux) {
		return jdbcTemplate.query("SELECT * from notes_jeux where pseudo_user = ? and id_jeux = ?", new Object[]{ pseudo_user, id_jeux }, new NoteJeuxRowMapper() );
		
	}    

}
