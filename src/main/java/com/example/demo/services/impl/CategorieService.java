package com.example.demo.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.example.demo.mapping.CategorieRowMapper;
import com.example.demo.modeles.Categorie;
import com.example.demo.services.ICategorieService;

@Service
public class CategorieService implements ICategorieService {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Categorie> findAllCategorie() {
		return jdbcTemplate.query("SELECT id, nom FROM categories", new CategorieRowMapper());
	}

	@Override
	public Categorie findCategorieByName(String name) {
		return jdbcTemplate.query("SELECT id, nom FROM categories WHERE nom = ?", new Object[] {name}, new CategorieRowMapper()).get(0);
	}

	@Override
	public List<Categorie> findCategorieById(int id) {
		return jdbcTemplate.query("SELECT id, nom FROM categories WHERE id = ?", new Object[] {id}, new CategorieRowMapper());
	}

}
