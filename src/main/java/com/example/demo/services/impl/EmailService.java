package com.example.demo.services.impl;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Component;

import com.example.demo.modeles.Mail;
import com.example.demo.services.IEmailService;

@Component
public class EmailService implements IEmailService {

	@Override
	public void WelcomeMail(String to) {
		String from = "david.sylvestre.lp2@gmail.com";
	      final String username = "david.sylvestre.lp2@gmail.com";//change accordingly
	      final String password = "Myriamelenajeremy24";//change accordingly

	      // Assuming you are sending email through relay.jangosmtp.net
	      String host = "smtp.gmail.com";

	      Properties props = new Properties();
	      props.put("mail.smtp.auth", "true");
	      props.put("mail.smtp.starttls.enable", "true");
	      props.put("mail.smtp.host", host);
	      props.put("mail.smtp.port", "587");

	      // Get the Session object.
	      Session session = Session.getInstance(props,
	         new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	               return new PasswordAuthentication(username, password);
	            }
		});

	      try {
	            // Create a default MimeMessage object.
	            Message message = new MimeMessage(session);

	   	   // Set From: header field of the header.
		   message.setFrom(new InternetAddress(from));

		   // Set To: header field of the header.
		   message.setRecipients(Message.RecipientType.TO,
	              InternetAddress.parse(to));

		   // Set Subject: header field
		   message.setSubject("Bienvenue sur apprendre en s'amusant.");

		   // Send the actual HTML message, as big as you like
		   message.setContent(Mail.MAIL_DE_BIENVENUE, "text/html");

		   // Send message
		   Transport.send(message);

		   System.out.println("Sent message successfully....");

	      } catch (MessagingException e) {
		   e.printStackTrace();
		   throw new RuntimeException(e);
	      }
		
	}

	@Override
	public void forgetMail(String to, int code) {
		String from = "david.sylvestre.lp2@gmail.com";
	      final String username = "david.sylvestre.lp2@gmail.com";//change accordingly
	      final String password = "Myriamelenajeremy24";//change accordingly

	      // Assuming you are sending email through relay.jangosmtp.net
	      String host = "smtp.gmail.com";

	      Properties props = new Properties();
	      props.put("mail.smtp.auth", "true");
	      props.put("mail.smtp.starttls.enable", "true");
	      props.put("mail.smtp.host", host);
	      props.put("mail.smtp.port", "587");

	      // Get the Session object.
	      Session session = Session.getInstance(props,
	         new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	               return new PasswordAuthentication(username, password);
	            }
		});

	      try {
	            // Create a default MimeMessage object.
	            Message message = new MimeMessage(session);

	   	   // Set From: header field of the header.
		   message.setFrom(new InternetAddress(from));

		   // Set To: header field of the header.
		   message.setRecipients(Message.RecipientType.TO,
	              InternetAddress.parse(to));

		   // Set Subject: header field
		   message.setSubject("Changez votre mot de passe");

		   // Send the actual HTML message, as big as you like
		   message.setContent(Mail.MAIL_PASSWORD_DEBUT+code+Mail.MAIL_PASSWORD_FIN, "text/html");

		   // Send message
		   Transport.send(message);

		   System.out.println("Sent message successfully....");

	      } catch (MessagingException e) {
		   e.printStackTrace();
		   throw new RuntimeException(e);
	      }
		
	}

	public void achatMail(String nomJeu, int prix, String to) {
		String from = "david.sylvestre.lp2@gmail.com";
	      final String username = "david.sylvestre.lp2@gmail.com";//change accordingly
	      final String password = "Myriamelenajeremy24";//change accordingly

	      // Assuming you are sending email through relay.jangosmtp.net
	      String host = "smtp.gmail.com";

	      Properties props = new Properties();
	      props.put("mail.smtp.auth", "true");
	      props.put("mail.smtp.starttls.enable", "true");
	      props.put("mail.smtp.host", host);
	      props.put("mail.smtp.port", "587");

	      // Get the Session object.
	      Session session = Session.getInstance(props,
	         new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	               return new PasswordAuthentication(username, password);
	            }
		});

	      try {
	            // Create a default MimeMessage object.
	            Message message = new MimeMessage(session);

	   	   // Set From: header field of the header.
		   message.setFrom(new InternetAddress(from));

		   // Set To: header field of the header.
		   message.setRecipients(Message.RecipientType.TO,
	              InternetAddress.parse(to));

		   // Set Subject: header field
		   message.setSubject("Merci pour votre achat");

		   // Send the actual HTML message, as big as you like
		   message.setContent(Mail.MAIL_ACHAT_DEBUT+nomJeu+Mail.MAIL_ACHAT_MILIEU+prix+Mail.MAIL_ACHAT_FIN, "text/html");

		   // Send message
		   Transport.send(message);

		   System.out.println("Sent message successfully....");

	      } catch (MessagingException e) {
		   e.printStackTrace();
		   throw new RuntimeException(e);
	      }
		
	}@Override
	public boolean contactMail(String mailFrom, String subject, String contenu) {
		String from = mailFrom;
	      final String username = "david.sylvestre.lp2@gmail.com";//change accordingly
	      final String password = "Myriamelenajeremy24";//change accordingly

	      // Assuming you are sending email through relay.jangosmtp.net
	      String host = "smtp.gmail.com";

	      Properties props = new Properties();
	      props.put("mail.smtp.auth", "true");
	      props.put("mail.smtp.starttls.enable", "true");
	      props.put("mail.smtp.host", host);
	      props.put("mail.smtp.port", "587");

	      // Get the Session object.
	      Session session = Session.getInstance(props,
	         new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	               return new PasswordAuthentication(username, password);
	            }
		});

	      try {
	            // Create a default MimeMessage object.
	            Message message = new MimeMessage(session);

	   	   // Set From: header field of the header.
		   message.setFrom(new InternetAddress(from));

		   // Set To: header field of the header.
		   message.setRecipients(Message.RecipientType.TO,
	              InternetAddress.parse("david.sylvestre.lp2@gmail.com"));

		   // Set Subject: header field
		   message.setSubject(subject);

		   // Send the actual HTML message, as big as you like
		   message.setContent(contenu + "\n\n" + mailFrom, "text/html");

		   // Send message
		   Transport.send(message);

		   System.out.println("Sent message successfully....");
		   return true;

	      } catch (MessagingException e) {
		   e.printStackTrace();
		   return false;
	      }
		
	}
}
