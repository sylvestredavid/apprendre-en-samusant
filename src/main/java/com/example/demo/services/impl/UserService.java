package com.example.demo.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.example.demo.BCrypt;
import com.example.demo.mapping.UserRowMapper;
import com.example.demo.modeles.User;
import com.example.demo.services.IUserService;

@Service
public class UserService implements IUserService {

	@Autowired
	JdbcTemplate jdbcTemplate;
	

	@Override
	public List<User> findAll() {
		return jdbcTemplate.query("SELECT id, pseudo, password, email FROM users", new UserRowMapper());
	}

	@Override
	public List<User> findOneUserById(int id) {
		return jdbcTemplate.query("SELECT id, pseudo, password, email FROM users WHERE id = ?", new Object[] { id },
				new UserRowMapper());
	}

	@Override
	public List<User> findOneUserByPseudo(String pseudo) {
		return jdbcTemplate.query("SELECT id, pseudo, password, email FROM users WHERE pseudo = ?",
				new Object[] { pseudo }, new UserRowMapper());
	}

	@Override
	public User saveUser(User user) {
		String password = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(4));
		jdbcTemplate.update("insert into users(pseudo, password, email) VALUES(?,?,?)", user.getPseudo(), password,
				user.getEmail());
		return user;
	}

	@Override
	public boolean verifyUser(User user) {
		List<User> userVerify = findOneUserByPseudo(user.getPseudo());

		if (userVerify.size() > 0 && BCrypt.checkpw(user.getPassword(), userVerify.get(0).getPassword())) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<User> findOneUserByMail(String mail) {
		return jdbcTemplate.query("SELECT id, pseudo, password, email FROM users WHERE email = ?",
				new Object[] { mail }, new UserRowMapper());
	}

	@Override
	public boolean verifyPassword(String password) {
		if (password.matches("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$")) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void changePassword(User user) {
		String password =  BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(4));
		jdbcTemplate.update("UPDATE users SET password=? WHERE email=?", password, user.getEmail());
		
	}

}
