package com.example.demo.mapping;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.demo.modeles.Categorie;
import com.example.demo.modeles.Jeu;

public class CategorieRowMapper implements RowMapper<Categorie> {

	@Override
	public Categorie mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Categorie(rs.getInt("id"), rs.getString("nom"));
	}

}
