package com.example.demo.mapping;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.demo.modeles.NoteJeux;

public class MoyenneRowMapper implements RowMapper<NoteJeux> {

	@Override
	public NoteJeux mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new NoteJeux(rs.getInt("id_jeux"), rs.getDouble("moyenne"));
	}

}
