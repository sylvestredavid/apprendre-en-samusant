package com.example.demo.mapping;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.demo.modeles.Categorie;
import com.example.demo.modeles.Jeu;

public class JeuRowMapper implements RowMapper<Jeu> {

	@Override
	public Jeu mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Jeu(rs.getInt("id"), rs.getString("nom"), rs.getString("photo"), rs.getString("lien"), rs.getInt("prix"), rs.getString("description"), new Categorie(rs.getInt("categories.id"), rs.getString("categories.nom")), rs.getDouble("moyenne"));
	}

}
