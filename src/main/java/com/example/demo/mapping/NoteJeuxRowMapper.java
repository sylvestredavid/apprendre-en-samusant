package com.example.demo.mapping;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import com.example.demo.modeles.Jeu;
import com.example.demo.modeles.NoteJeux;

public class NoteJeuxRowMapper implements RowMapper<NoteJeux> {

	@Override
	public NoteJeux mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new NoteJeux(rs.getString("pseudo_user"), rs.getInt("id_jeux"), rs.getDouble("note"), rs.getString("commentaire"), rs.getString("datecom"));
	}

	

}
