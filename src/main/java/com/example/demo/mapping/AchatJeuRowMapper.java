package com.example.demo.mapping;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.demo.modeles.AchatJeu;
import com.example.demo.modeles.NoteJeux;

public class AchatJeuRowMapper implements RowMapper<AchatJeu> {

	@Override
	public AchatJeu mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new AchatJeu(rs.getInt("id_user"), rs.getInt("id_jeux"));
	}

}
