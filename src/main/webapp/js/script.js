$(function() {

	$("#note1").click(function(e){noter(e, 5)});
	$("#note2").click(function(e){noter(e, 4)});
	$("#note3").click(function(e){noter(e, 3)});
	$("#note4").click(function(e){noter(e, 2)});
	$("#note5").click(function(e){noter(e, 1)});

	moyenne();
});

function moyenne() {
	var moyenne = $("#moyenne").val();
	var count = 0.5;

	while (count <= moyenne) {
		$("#star" + (count * 2)).css("color", "#FCDC12");
		count = count + 0.5;
	}
}

function noter(e, note){
	var com = prompt("entrez votre commentaire");
	if (com == "" || com != null) {
		$("#commentaire").val(com);
		$("#note").val(note);

		// Prevent default submission of form
		e.preventDefault();

		$.post({
			url : 'jeu',
			data : $('form[name=commentForm]').serialize(),
			success : function(res) {
				$("#noter").remove();
				$("#noteForm").load("jeu?nom=" + $("#lien").val() + " #votreNote");
				$("#commentaires").remove();
				$("#commentairesDiv").load("jeu?nom=" + $("#lien").val() + " #commentaires");
				alert("Merci pour votre note.")
			}
		})
	}
	else{
		$("#note").val("noter");
	}
}