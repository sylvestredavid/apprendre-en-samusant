<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="wrapper">
		<jsp:include page="${request.contextPath}/menu"></jsp:include>
		<div class="main">
			<h1 class="jeuxPref-title">jeux preferes de ${ userSession.pseudo }</h1>
			<div class="jeuxPref-body" id="jeuxPref">
				<c:forEach items="${ mesJeux }" var="jeu">
					<div class="jeuxPref-item">
						<a href="jeu?nom=${ jeu.lien }" class="jeuxPref-link">
							<figure class="jeuxPref-item-figure">
								<img alt="${ jeu.nom }" src="img/${ jeu.photo }" width="200px" class="jeuxPref-item-img">
							</figure>
							<p class="jeuxPref-item-title">${ jeu.nom } - ${ jeu.moyenne }<span class="material-icons">star</span></p>
						</a>
						</div>
				</c:forEach>
			</div>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</body>
</html>