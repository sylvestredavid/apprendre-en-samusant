<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="wrapper">
		<jsp:include page="${request.contextPath}/menu"></jsp:include>
		<div class="main">
		<h1 class="contact-title">Contact</h1>
			<form method="POST" action="contact" class="form" name="contactForm">
				<label for="email" class="form-label">Votre email</label>
				<input type="email" name="mailFrom" id="mailFrom" placeholder="email" class="form-input">
				<label for="subject" class="form-label">Sujet</label>
				<input type="text" name="subject" id="subject" placeholder="Sujet" class="form-input">
				<label for="message" class="form-label">Votre message</label>
				<textArea name="contenu" id="contenu" placeholder="Message" class="form-input"></textArea>
				<input type="submit" class="form-submit" value="Envoyer">
				<p class="erreur">${ resultat }</p>
			</form> 
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>