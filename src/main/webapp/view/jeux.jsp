<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="wrapper">
		<jsp:include page="${request.contextPath}/menu"></jsp:include>
		<div class="main">
			<h1 class="jeux-title">${ !empty categorie ? categorie : 'Tout les jeux' }</h1>
			<div class="jeux-body">
				<c:forEach items="${ jeux }" var="jeu">
					<div class="jeux-item">
						<a class="jeux-item-link" href="jeu?nom=${jeu.lien }">
							<figure class="jeux-item-figure">
								<img alt="${ jeu.nom }" src="img/${ jeu.photo }"
									class="jeux-item-img" />
								<c:if test="${ jeu.prix > 0 }">
									<p class="jeux-prix">${ jeu.prix/100 }0€</p>
								</c:if>
							</figure>
							<p class="jeux-item-title">${ jeu.nom } - ${ jeu.moyenne }<span class="material-icons">star</span></p>
						</a>
					</div>
				</c:forEach>
			</div>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>