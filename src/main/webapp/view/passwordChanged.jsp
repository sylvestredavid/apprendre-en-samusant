<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Person Form</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="wrapper">
		<jsp:include page="${request.contextPath}/menu"></jsp:include>
		<div class="main">
			<h1 class="form-title">Mot de passe changé</h1>
			<p>Vous pouvez à présent vous connecter avec votre nouveau mot de passe.</p>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>