<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="wrapper">
		<jsp:include page="${request.contextPath}/menu"></jsp:include>
		<div class="main">
			<h1 class="about-title">A propos de nous</h1>
			<div class="about-body">
				<p>Apprendre en s'amusant est un site appartenant a la société Djem games, spécialisée dans la création de jeux vidéos.</p>

				<p>Apprendre en s’amusant a pour but de permettre aux enfants d’apprendre les bases de la lecture et de l’écriture, mais aussi des maths est bien plus, tout en s’amusant.</p>

				<p>Djem games a été créée par David Sylvestre, passionné d’informatique, de codage et de jeux vidéos. L’idée du site apprendre-en-samusant.com lui est venue en regardant sa fille de 5ans jouer à des jeux sur un téléphone portable, il s’est alors dit que tant qu’a jouer, autant qu’elle joue a des jeux qui lui apportent quelque chose. </p>
			</div>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>