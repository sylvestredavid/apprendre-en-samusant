<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<header class="header">
		<nav class="header-nav">
		<a href="index"><img class="header-img" alt="logo" src="img/logo.png"></a>
			<ul class="header-menu-left">
				<c:forEach items="${ categories }" var="categorie">
					<li class="header-menu-item"><a class="header-menu-link" href="jeux?cat=${ categorie.id }">${ categorie.nom }</a></li>
				</c:forEach>
				<li class="header-menu-item"><a class="header-menu-link" href="jeux">Tout les jeux</a></li>
			</ul>
			<ul class="header-menu-right">
				<c:choose>
					<c:when test="${ !empty userSession }">
						<li class="header-menu-item"><a class="header-menu-link" href="logout">Se deconnecter</a></li>
						<li class="header-menu-item"><a class="header-menu-link" href="mesJeux">Mes jeux</a></li>
					</c:when>
					<c:otherwise>
						<li class="header-menu-item"><a class="header-menu-link" href="logon">S'inscrire</a></li>
						<li class="header-menu-item"><a class="header-menu-link" href="login">Se connecter</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</nav>
	</header>
	<div class="clear"></div>
</body>
</html>