<footer class="footer">
	<ul class="footer-menu">
		<li class="footer-menu-item"><a class="footer-menu-link" href="#">Conditions generales</a></li>
		<li class="footer-menu-item"><a class="footer-menu-link" href="contact">Contact</a></li>
		<li class="footer-menu-item"><a class="footer-menu-link" href="about">A propos de nous</a></li>
	</ul>
	<img class="footer-img" alt="logo" src="img/logo.png">
</footer>