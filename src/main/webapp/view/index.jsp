<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="wrapper">
		<jsp:include page="${request.contextPath}/menu"></jsp:include>
		<div class="main">
			<div class="welcom">
				<h1 class="welcom-title">Bienvenue sur apprendre en s'amusant</h1>
				<p class="welcom-text">
					Afin de profiter pleinement des jeux, il est conseillé d'utiliser <a
						href="https://www.mozilla.org/fr/firefox/new/" target="_blank">Firefox</a>
					ou <a href="https://www.google.com/intl/fr_ALL/chrome/"
						target="_blank">google chrome</a>.<br> Amusez-vous bien !
				</p>
			</div>
			<div class="jeuALaUne">
				<h2 class="jeuALaUne-title">Jeu à la une</h2>
				<a class="" href="jeu?nom=${ jeuALaUne.lien }">
					<figure class="jeuALaUne-figure">
						<img class="jeuALaUne-img" alt="${ jeuALaUne.nom }"
							src="img/${ jeuALaUne.photo }">
						<c:if test="${ jeuALaUne.prix > 0 }">
							<p class="jeuALaUne-prix">${ jeuALaUne.prix/100 }0€</p>
						</c:if>
					</figure>
				</a>
				<div class="jeuALaUne-body">
					<h3 class="jeuALaUne-body-title">${ jeuALaUne.nom }</h3>
					<div class="jeuALaUne-note">
						<input type="hidden" id="moyenne" value="${ jeuALaUne.moyenne }">
						<div class="star_note">
							<i id="star1" class="material-icons star_gauche">star</i>
						</div>
						<div class="star_note">
							<i id="star2" class="material-icons star_droite">star</i>
						</div>
						<div class="star_note">
							<i id="star3" class="material-icons star_gauche">star</i>
						</div>
						<div class="star_note">
							<i id="star4" class="material-icons star_droite">star</i>
						</div>
						<div class="star_note">
							<i id="star5" class="material-icons star_gauche">star</i>
						</div>
						<div class="star_note">
							<i id="star6" class="material-icons star_droite">star</i>
						</div>
						<div class="star_note">
							<i id="star7" class="material-icons star_gauche">star</i>
						</div>
						<div class="star_note" id="star">
							<i id="star8" class="material-icons star_droite">star</i>
						</div>
						<div class="star_note">
							<i id="star9" class="material-icons star_gauche">star</i>
						</div>
						<div class="star_note">
							<i id="star10" class="material-icons star_droite">star</i>
						</div>
						<div class="clear"></div>
					</div>
					<a class="jeuALaUne-body-link"
						href="jeux?cat=${ jeuALaUne.categorie.id }">${jeuALaUne.categorie.nom }</a>
					<p class="jeuALaUne-body-text">${jeuALaUne.description }</p>
				</div>
				<div class="clear"></div>
			</div>
			<div class="topJeux">
				<h2 class="topJeux-title">Meilleurs jeux</h2>
				<div class="topJeux-body">
					<c:forEach items="${ topJeux }" var="topJeu">
						<div class="topJeux-item">
							<a class="topJeux-item-link" href="jeu?nom=${ topJeu.lien }">
								<figure class="topJeux-item-figure">
									<img alt="${ topJeu.nom }" src="img/${ topJeu.photo }"
										class="topJeux-item-img" />
									<c:if test="${ topJeu.prix > 0 }">
										<p class="topJeu-prix">${ topJeu.prix/100 }0€</p>
									</c:if>
								</figure>
								<p class="topJeux-item-title">${ topJeu.nom } - ${ topJeu.moyenne }<span class="material-icons">star</span></p>
							</a>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</body>
</html>