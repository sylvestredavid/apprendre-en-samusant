<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta name="google-signin-client_id" content="657601800153-qmbojlb2etaqggv09tiim4ofv911d22o.apps.googleusercontent.com">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Person Form</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script type="text/javascript" src="js/fb.js"></script>
</head>
<body>
	<div class="wrapper">
		<jsp:include page="${request.contextPath}/menu"></jsp:include>
		<div class="main">
			<h1 class="form-title">S'inscrire</h1>
			<div class="social_login">
				<div id="my-signin2">Google</div>
				<fb:login-button class="fb-login-button" data-size="xlarge" scope="public_profile,email"onlogin="checkLoginState();">Facebook</fb:login-button>
  			</div>
  			<div class="mail_login">
				<form:form modelAttribute="user" action="logon" method="post"
					class="form">
					<form:label path="pseudo" class="form-label">Pseudo</form:label>
					<form:input path="pseudo" type="text" placeholder="pseudo"
						class="form-input" />
					<form:label path="email" class="form-label">Email</form:label>
					<form:input path="email" type="email" placeholder="email"
						class="form-input" />
					<form:label path="password" class="form-label">Mot de passe</form:label>
					<form:input path="password" type="password"
						placeholder="mot de passe" class="form-input" />
					<form:label path="password2" for="password2" class="form-label">Ré-écrivez votre mot de passe</form:label>
					<form:input path="password2" id="password2" type="password"
						placeholder="mot de passe" class="form-input" />
					<input type="submit" class="form-submit" value="S'inscrire">
					<p class="erreur">${ erreur }</p>
				</form:form>
			</div>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
  <script>
    function onSuccess(googleUser) {
    	// The ID token you need to pass to your backend:
        var id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);
        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'googleSignIn');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
          console.log('Signed in as: ' + xhr.responseText);
          window.location.replace("index");
        };
        xhr.send('idtoken=' + id_token);
    }
    function onFailure(error) {
      console.log(error);
    }
    function renderButton() {
      gapi.signin2.render('my-signin2', {
        'scope': 'profile email',
        'width': 160,
        'height': 40,
        'longtitle': false,
        'theme': 'light',
        'onsuccess': onSuccess,
        'onfailure': onFailure
      });
    }
  </script>

  <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
</body>
</html>