<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="wrapper">
		<jsp:include page="${request.contextPath}/menu"></jsp:include>
		<div class="main">
			<h1 class="jeu-title">${ jeu.nom }</h1>
			<p class="prix_jeu">${ jeu.prix/100 }0€</p>
			<div class="webgl-content">
				<div class="gameContainer">
					<img class="gameContainer" src="img/${ jeu.photo }"
						alt="${ jeu.nom }">
				</div>
				<div class="jeu-note">
					<input type="hidden" id="moyenne" value="${ jeu.moyenne }">
					<div class="star_note">
						<i id="star1" class="material-icons star_gauche">star</i>
					</div>
					<div class="star_note">
						<i id="star2" class="material-icons star_droite">star</i>
					</div>
					<div class="star_note">
						<i id="star3" class="material-icons star_gauche">star</i>
					</div>
					<div class="star_note">
						<i id="star4" class="material-icons star_droite">star</i>
					</div>
					<div class="star_note">
						<i id="star5" class="material-icons star_gauche">star</i>
					</div>
					<div class="star_note">
						<i id="star6" class="material-icons star_droite">star</i>
					</div>
					<div class="star_note">
						<i id="star7" class="material-icons star_gauche">star</i>
					</div>
					<div class="star_note">
						<i id="star8" class="material-icons star_droite">star</i>
					</div>
					<div class="star_note">
						<i id="star9" class="material-icons star_gauche">star</i>
					</div>
					<div class="star_note">
						<i id="star10" class="material-icons star_droite">star</i>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<p class="jeuDescription">${ jeu.description }</p>
			<div class="paiement">
				<c:choose>
					<c:when test="${ userSession == null}">
						<c:redirect url="login" />
					</c:when>
					<c:otherwise>
						<form action="paiement" method="POST">
							<script src="https://checkout.stripe.com/checkout.js"
								class="stripe-button"
								data-key="pk_test_DmX8gyXCy29ssGuYoNhk7AoN"
								data-amount="${ jeu.prix }" data-name="Apprendre en s'amusant"
								data-description="${ jeu.nom }"
								data-image="http://image.noelshack.com/fichiers/2019/02/3/1547069641-sans-titre.png"
								data-locale="auto" data-currency="eur">
								
							</script>
							<input type="hidden" name="id_jeux" value="${ jeu.id }" /> <input
								type="hidden" name="id_user" value="${ userSession.id }" /> <input
								type="hidden" name="description"
								value=" achat du jeu '${ jeu.nom }'" />
						</form>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="commentaires">
				<c:forEach items="${ commentaires }" var="commentaire">
					<div class="commentaires-body">
						<p class="commentaires-title">${ commentaire.pseudo_user }</p>
						<p class="commentaires-note">${ commentaire.note }<span class="material-icons">star</span></p>
						<div class="clear"></div>
						<p class="commentaires-text">${ commentaire.commentaire }</p>
					</div>
				</c:forEach>
			</div>
			<c:if test="${ !empty autresJeux }">
				<h3 class="autreJeux-title">Autres jeux de la categorie ${ jeu.categorie.nom }</h3>
				<div class="topJeux-body">
					<c:forEach items="${ autresJeux }" var="autreJeu">
						<div class="topJeux-item">
							<a class="topJeux-item-link" href="jeu?nom=${ autreJeu.lien }">
								<figure class="topJeux-item-figure">
									<img alt="${ autreJeu.nom }" src="img/${ autreJeu.photo }"
										class="topJeux-item-img" />
									<c:if test="${ autreJeu.prix > 0 }">
										<p class="jeux-prix">${ autreJeu.prix/100 }0€</p>
									</c:if>
								</figure>
								<p class="topJeux-item-title">${ autreJeu.nom }
									- ${ autreJeu.moyenne }<span class="material-icons">star</span>
								</p>
							</a>
						</div>
					</c:forEach>
				</div>
			</c:if>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</body>
</html>