<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Person Form</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="wrapper">
		<jsp:include page="${request.contextPath}/menu"></jsp:include>
		<div class="main">
			<form:form modelAttribute="user" action="passwordChanged" method="post"
				class="form" name="newPasswordForm">
				<h1 class="form-title">Changer de mot de passe</h1>
				<form:input path="email" type="hidden" value="${ email }" />
				<form:label path="password" class="form-label">entrez votre nouveau mot de passe</form:label>
				<form:input path="password" type="password"
					placeholder="nouveau mot de passe" class="form-input" />
				<input type="submit" class="form-submit" value="Ok" id="newPassword">
			</form:form>
				<p id="result"></p>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>