<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>apprendre en s'amusant | ${ jeu.nom }</title>
<meta property="og:url"           content="http://localhost:8080/apprendre-en-samusant/jeu?nom=${ jeu.lien }" />
<meta property="og:type"          content="apprend-en-samusant.com" />
<meta property="og:title"         content="${ jeu.nom }" />
<meta property="og:description"   content="${ jeu.description }" />
<meta property="og:image"         content="http://localhost:8080/apprendre-en-samusant/img/${ jeu.photo }" />
<script src="TemplateData/UnityProgress.js"></script>
<script src="Build/UnityLoader.js"></script>
<script>
	var gameInstance = UnityLoader.instantiate("gameContainer",
			"Build/${ jeu.lien }.json", {
				onProgress : UnityProgress
			});
</script>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/style.css" rel="stylesheet" type="text/css">
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
</head>
<body>
	<div class="wrapper">
		<jsp:include page="${request.contextPath}/menu"></jsp:include>
		<div class="main">
			<h1 class="jeu-title">${ jeu.nom }</h1>
			<div id="webgl" class="webgl-content">
				<div id="gameContainer" class="gameContainer"></div>
				<i class="material-icons fullscreen"
					onclick="gameInstance.SetFullscreen(1)"> zoom_out_map </i>
				 <div class="fb-share-button" data-size="large"
    data-href="http://localhost:8080/apprendre-en-samusant/jeu?nom=${ jeu.lien }" 
    data-layout="button_count"></div>
				<div class="jeu-note" id="jeu-note">
					<input type="hidden" id="moyenne" value="${ jeu.moyenne }">
					<div class="star_note">
						<i id="star1" class="material-icons star_gauche">star</i>
					</div>
					<div class="star_note">
						<i id="star2" class="material-icons star_droite">star</i>
					</div>
					<div class="star_note">
						<i id="star3" class="material-icons star_gauche">star</i>
					</div>
					<div class="star_note">
						<i id="star4" class="material-icons star_droite">star</i>
					</div>
					<div class="star_note">
						<i id="star5" class="material-icons star_gauche">star</i>
					</div>
					<div class="star_note">
						<i id="star6" class="material-icons star_droite">star</i>
					</div>
					<div class="star_note">
						<i id="star7" class="material-icons star_gauche">star</i>
					</div>
					<div class="star_note">
						<i id="star8" class="material-icons star_droite">star</i>
					</div>
					<div class="star_note">
						<i id="star9" class="material-icons star_gauche">star</i>
					</div>
					<div class="star_note">
						<i id="star10" class="material-icons star_droite">star</i>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<p class="jeuDescription">${ jeu.description }</p>
			<c:if test="${ !empty userSession }">
			<div id="noteForm">
			<c:choose>
				<c:when test="${ empty note }">
					<form action="jeu" method="POST" name="commentForm" id="commentForm">
						<input type="hidden" name="pseudo_user" id="pseudo_user" value="${ userSession.pseudo }"> 
						<input type="hidden" name="id_jeux" id="id_jeux" value="${ jeu.id }"> 
						<input type="hidden" name="lien" id="lien" value="${ jeu.lien }">
						<input type="hidden" name="note" id="note" >
						<input type="hidden" name="commentaire" id="commentaire">
					</form>
					<div id="noter" class="rating"><!--
					  --><a id="note1" class="material-icons">star</a><!--
					  --><a id="note2" class="material-icons">star</a><!--
					  --><a id="note3" class="material-icons">star</a><!--
					  --><a id="note4" class="material-icons">star</a><!--
					  --><a id="note5" class="material-icons">star</a>
					</div>
					<div class="clear"></div>
				</c:when>
				<c:otherwise>
					<p id="votreNote" class="votreNote">votre note : ${ note.note }<span class="material-icons">star</span></p>
				</c:otherwise>
			</c:choose>
				</div>
			</c:if>
			<div id="commentairesDiv">
				<div class="commentaires" id="commentaires">
					<c:forEach items="${ commentaires }" var="commentaire">
						<div class="commentaires-body">
							<p class="commentaires-title">${ commentaire.pseudo_user } le
								${ commentaire.date }</p>
							<p class="commentaires-note">${ commentaire.note }<span
									class="material-icons">star</span>
							</p>
							<div class="clear"></div>
							<p class="commentaires-text">${ commentaire.commentaire }</p>
						</div>
					</c:forEach>
				</div>
			</div>
			<c:if test="${ !empty autresJeux }">
				<h3 class="autreJeux-title">Autres jeux de la categorie ${ jeu.categorie.nom }</h3>
				<div class="topJeux-body">
					<c:forEach items="${ autresJeux }" var="autreJeu">
						<div class="topJeux-item">
							<a class="topJeux-item-link" href="jeu?nom=${ autreJeu.lien }">
								<figure class="topJeux-item-figure">
									<img alt="${ autreJeu.nom }" src="img/${ autreJeu.photo }"
										class="topJeux-item-img" />
									<c:if test="${ autreJeu.prix > 0 }">
										<p class="jeux-prix">${ autreJeu.prix/100 }0€</p>
									</c:if>
								</figure>
								<p class="topJeux-item-title">${ autreJeu.nom }
									- ${ autreJeu.moyenne }<span class="material-icons">star</span>
								</p>
							</a>
						</div>
					</c:forEach>
				</div>
			</c:if>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</body>
</html>