<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Person Form</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="wrapper">
		<jsp:include page="${request.contextPath}/menu"></jsp:include>
		<div class="main">
			<form:form modelAttribute="user" action="forgot" method="get"
				class="form">
				<h1 class="form-title">Mot de passe oublié</h1>
				<form:label path="email" class="form-label">Entre votre email</form:label>
				<form:input path="email" type="text" placeholder="pseudo"
					class="form-input"/>
				<input type="submit" class="form-submit" value="Ok">
				<p class="erreur">${ erreur }</p>
			</form:form>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>