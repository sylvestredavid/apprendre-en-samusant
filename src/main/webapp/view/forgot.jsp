<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Person Form</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="wrapper">
		<jsp:include page="${request.contextPath}/menu"></jsp:include>
		<div class="main">
			<form:form modelAttribute="user" action="newPassword" method="post"
				class="form">
				<h1 class="form-title">Mot de passe oublié</h1>
				<p>Nous vous avons envoyé un email avec un code de confirmation</p>
				<form:input path="email" type="hidden" value="${ email }" />
				<form:input path="codeAVerifier" type="hidden" value="${ code }" />
				<form:label path="code" class="form-label">entrez le code de validation</form:label>
				<form:input path="code" type="code"
					placeholder="code d'activation" class="form-input" />
				<input type="submit" class="form-submit" value="Ok">
				<p class="erreur">${ erreur }</p>
			</form:form>
			<a href="forget">mot de passe oublié.</a>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>